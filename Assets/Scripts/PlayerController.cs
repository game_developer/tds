﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    VirtualJoystick _virtualJoystick;

    [SerializeField] float _moveSpeed;
    Rigidbody2D _rb;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _virtualJoystick = FindObjectOfType<VirtualJoystick>();
    }

    void FixedUpdate()
    {
        if (_virtualJoystick.InputDirection != default(Vector2))
        {
            var angle = Mathf.Atan2(_virtualJoystick.InputDirection.y, _virtualJoystick.InputDirection.x) * Mathf.Rad2Deg;

            _rb.MoveRotation(angle);
            _rb.MovePosition(_rb.position + _virtualJoystick.InputDirection * _moveSpeed * Time.deltaTime);
        }
    }
}
