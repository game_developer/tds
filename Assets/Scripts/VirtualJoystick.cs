﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    Image _bgImage;
    Image _joystickImg;
    public Vector2 InputDirection { get; private set; }

    float _halfBgWidth;
    float _halfBgHeight;

    void Start()
    {
        _bgImage = GetComponent<Image>();
        _joystickImg = transform.GetChild(0).GetComponent<Image>();

        _halfBgWidth = _bgImage.rectTransform.sizeDelta.x / 2;
        _halfBgHeight = _bgImage.rectTransform.sizeDelta.y / 2;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_bgImage.rectTransform, eventData.position,
            eventData.pressEventCamera, out pos))
        {
            pos.x -= _halfBgWidth;
            pos.y -= _halfBgHeight;

            var magnitude = pos.magnitude;
            var scale = magnitude / (_halfBgWidth * 0.6f);

            if (scale > 1)
            {
                pos /= scale;
            }

            _joystickImg.rectTransform.anchoredPosition = pos;

            InputDirection = pos.normalized;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // TODO: Lerp the movement to the origin
        _joystickImg.rectTransform.anchoredPosition = new Vector2();

        InputDirection = new Vector3();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }
}
